/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen01poo;

/**
 *
 * @author Iker Martinez
 */
public class Recibo2 {
    private int numRecibo ;
    private String fecha;
    private String nom;
    private String dom;
    private int tipo;
    private float costKw;
    private int kwConsu;

    public Recibo2(){
        this.numRecibo = 103;
        this.fecha = "21 de marzo 2019";
        this.nom = "Abarrotes feliz";
        this.dom = "Av del sol ";
        this.tipo = 2;
        this.costKw = 3.00f;
        this.kwConsu = 1200;
    }

    public Recibo2(int numRecibo, String fecha, String nom, int tipo, float costKw, int kwConsu) {
        this.numRecibo = numRecibo;
        this.fecha = fecha;
        this.nom = nom;
        this.tipo = tipo;
        this.costKw = costKw;
        this.kwConsu = kwConsu;
    }
     
    public Recibo2(Recibo2 otro) {
        this.numRecibo = otro.numRecibo;
        this.fecha = otro.fecha;
        this.nom = otro.nom;
        this.tipo = otro.tipo;
        this.costKw = otro.costKw;
        this.kwConsu = otro.kwConsu;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getCostKw() {
        return costKw;
    }

    public void setCostKw(float costKw) {
        this.costKw = costKw;
    }

    public int getKwConsu() {
        return kwConsu;
    }

    public void setKwConsu(int kwConsu) {
        this.kwConsu = kwConsu;
    }
    
    public float calcularSubtotal() {
        float subtotal = 0.0f;
        subtotal = this.kwConsu * this.costKw;
        return subtotal;                  
    }
    public float calcularImpuesto(){
    float impuesto = 0.0f;
    float sub = calcularSubtotal();
    impuesto = sub * 0.16f;
    return impuesto;
    }
    public float calcularTotalPagar(){
    float total = 0.0f;
    float sub = calcularSubtotal();
    float impuesto = calcularImpuesto();
    total = sub + impuesto;
    return total;
    }
    
    
}


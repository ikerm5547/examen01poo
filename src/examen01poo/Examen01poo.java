/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen01poo;

/**
 *
 * @author Iker Martinez
 */
public class Examen01poo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Recibo re = new Recibo();
        
        System.out.println("Numero de recibo: " + re.getNumRecibo());
        System.out.println("Fecha: " + re.getFecha());
        System.out.println("Nombre: " + re.getNom());
        System.out.println("Domicilio: " + re.getDom());
        System.out.println("Tipo de servicio: " + re.getTipo());
        System.out.println("Costo por kilowatts: " + re.getCostKw());
        System.out.println("Kiowatts consumido: " + re.getKwConsu());
        System.out.println("SUBTOTAL: " + re.calcularSubtotal());
        System.out.println("IMPUESTO (16%): " + re.calcularImpuesto());
        System.out.println("TOTAL A PAGAR: " + re.calcularTotalPagar());
         System.out.println("//////////////////////////////////////////////////");
        Recibo2 re2 = new Recibo2();
        System.out.println("Numero de recibo: " + re2.getNumRecibo());
        System.out.println("Fecha: " + re2.getFecha());
        System.out.println("Nombre: " + re2.getNom());
        System.out.println("Domicilio: " + re2.getDom());
        System.out.println("Tipo de servicio: " + re2.getTipo());
        System.out.println("Costo por kilowatts: " + re2.getCostKw());
        System.out.println("Kiowatts consumido: " + re2.getKwConsu());
        System.out.println("SUBTOTAL: " + re2.calcularSubtotal());
        System.out.println("IMPUESTO (16%): " + re2.calcularImpuesto());
        System.out.println("TOTAL A PAGAR: " + re2.calcularTotalPagar());
        
    }
    
}
